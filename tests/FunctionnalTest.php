<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FunctionalTest extends WebTestCase
{
    public function testShouldDisplayVoitureIndex()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/voiture');
        $this->assertResponseIsSuccessful();

        $this->assertSelectorTextContains('h1', 'Voiture index');
    }

    public function testShouldDisplayCreateNewVoiture()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/voiture/new');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Create new Voiture');
    }

    public function testShouldAddNewVoiture()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/voiture/new');
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        $uuid = uniqid();

        $form = $buttonCrawlerNode->form([
            'voiture[serie]' => 'Add Voiture For Test' . $uuid,
            'voiture[date_mise_en_marche][year]' => '2022',
            'voiture[date_mise_en_marche][month]' => '01',
            'voiture[date_mise_en_marche][day]' => '05',
            'voiture[modele]' => 'add voiture modele',
            'voiture[prix_jour]' => '123.1',
        ]);
        $client->submit($form);

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('body', 'Add Voiture For Test' . $uuid);
    }
}

