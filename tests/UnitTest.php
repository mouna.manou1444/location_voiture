<?php

namespace App\Tests;

use App\Entity\Client;
use App\Entity\Location;
use App\Entity\Voiture;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{
    public function testClientProperties()
    {
        // Crée une instance de la classe Client
        $client = new Client();

        // Définit les valeurs des propriétés
        $client->setCin('123456789');
        $client->setNom('Doe');
        $client->setPrenom('John');
        $client->setAdresse('123 Main Street');

        // Vérifie que les valeurs des propriétés correspondent aux valeurs définies
        $this->assertEquals('123456789', $client->getCin());
        $this->assertEquals('Doe', $client->getNom());
        $this->assertEquals('John', $client->getPrenom());
        $this->assertEquals('123 Main Street', $client->getAdresse());
    }
}
class LocationTest extends TestCase
{
    public function testLocation()
    {
        // Crée une instance de la classe Location
        $location = new Location();

        // Définit les valeurs des propriétés date_debut, date_retour, et prix
        $dateDebut = new \DateTime('2024-01-01');
        $dateRetour = new \DateTime('2024-01-10');
        $prix = 100.00;

        $location->setDateDebut($dateDebut);
        $location->setDateRetour($dateRetour);
        $location->setPrix($prix);

        // Vérifie que les valeurs sont correctement définies
        $this->assertEquals($dateDebut, $location->getDateDebut());
        $this->assertEquals($dateRetour, $location->getDateRetour());
        $this->assertEquals($prix, $location->getPrix());
    }
}
class VoitureTest extends TestCase
{
    public function testCreationVoiture()
    {
        // Crée une instance de la classe Voiture
        $voiture = new Voiture();

        // Défini des valeurs pour les propriétés
        $serie = '12345';
        $dateMiseEnMarche = new \DateTime('2022-01-01');
        $modele = 'Sedan';
        $prixJour = 50.0;

        $voiture->setSerie($serie);
        $voiture->setDateMiseEnMarche($dateMiseEnMarche);
        $voiture->setModele($modele);
        $voiture->setPrixJour($prixJour);

        // Vérifie que les valeurs ont été correctement définies
        $this->assertEquals($serie, $voiture->getSerie());
        $this->assertEquals($dateMiseEnMarche, $voiture->getDateMiseEnMarche());
        $this->assertEquals($modele, $voiture->getModele());
        $this->assertEquals($prixJour, $voiture->getPrixJour());
    }
}
